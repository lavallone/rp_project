#include "path_matrix_astar_global_search.h"

namespace srrg2_navigation_2d {
  using namespace srrg2_core;
  using namespace std;

  void PathMatrixAstarGlobalSearch::reset() {

    if (!_path_map) cerr << "[PathMatrixAstarGlobalSearch::reset] please set a path map" << endl;

    const size_t& r = _path_map->rows();
    const size_t& c = _path_map->cols();
    if (r < 3 || c < 3) cerr << "[PathMatrixAstarGlobalSearch::reset] map is too small to compute a distance map" << endl;

    for (size_t i = 0; i < _path_map->size(); ++i) {
      PathMatrixCell& cell = _path_map->at(i);
      cell.parent = nullptr;
      cell.cost = numeric_limits<float>::max();
    }
    _queue = PathSearchQueue();
    _path_map_changed_flag = false;
  }

  int PathMatrixAstarGlobalSearch::setGoal(const Vector2i& goal_) { 

    if (!_path_map->inside(goal_)){
      cerr << "goal outside _path_map: " << goal_.transpose() << std::endl;
      return false;
    }
    _goal=goal_;
    _path_map_changed_flag = false;
    return true;
  }

  bool PathMatrixAstarGlobalSearch::setStart(const Vector2i& start_) {
    if (_path_map_changed_flag) reset();
      
    if (!_path_map->inside(start_)) {
      cerr << "start point outside _path_map: " << start_.transpose() << std::endl;
      return false;
    }

    PathMatrixCell& cell = _path_map->at(start_);
    if (cell.distance < 0) return false;

    cell = PathMatrixCell(cell.distance, 0.0f, cell.cost, &cell);
    _queue.push(PathSearchEntry(cell.heuristic+cell.cost, &cell));

    _path_map_changed_flag = false;
    _search_param_changed_flag = true; //la flag indica che stiamo iniziando l'algoritmo di ricerca A*!

    return true;
  }

  void PathMatrixAstarGlobalSearch::compute() {
    if (_path_map_changed_flag) reset();

    if (_queue.empty()) cerr <<  "[PathMatrixAstarGlobalSearch::reset] queue is empty" << endl;

    int exp = 0; //numero di espansioni id A*
    int max_exp = 10000; //mi do un limite
    PathMatrixCell* current_cell = nullptr;
    const int* neighbor_offsets = _path_map->eightNeighborOffsets();
    _status = PathMatrixCostSearch::SearchStatus::GoalNotFound;

    float traversal_cost = 0;
    float g = 0;
    float f = 0;

    /* 
    La funzione di costo che viene utilizzata per scegliere quale cella espandere nella ricerca è f = g + h. 
    Con g che indica la distanza esatta dalla posizione iniziale e h l'euristica che stima (mai overstima!) la distanza per raggiungere il goal.
    */

    // la ricerca termina se abbiamo raggiunto il goal, se abbiamo superato il numero di espansioni massimo o se la lista di posizioni da espandere è vuota (non c'è più nulla da espandere) 
    while ( _status == PathMatrixCostSearch::SearchStatus::GoalNotFound && exp < max_exp && !_queue.empty()) {

      PathSearchEntry e = _queue.top();
      current_cell = e.cell;
      _queue.pop();
      Vector2i current_pos = _path_map->pos(current_cell);

      if (current_pos == _goal) { //abbiamo trovato il goal!
        _status = PathMatrixCostSearch::SearchStatus::GoalFound;
        break; 
      }

      for (int i = 0; i < 8; ++i) {
        auto neighbor_cell = current_cell + neighbor_offsets[i];
        Vector2i neighbor_pos = _path_map->pos(neighbor_cell);
        int d = (neighbor_pos - current_pos).squaredNorm();
        
        if (d > 1) traversal_cost = traversalCost(sqrt(2.f), neighbor_cell->distance);
        else traversal_cost = traversalCost(1, neighbor_cell->distance);

        g = current_cell->cost + traversal_cost;
        if (g < neighbor_cell->cost) { 
          neighbor_cell->cost = g;
          neighbor_cell->parent = current_cell;
          f = neighbor_cell->cost + neighbor_cell->heuristic;
          _queue.push(PathSearchEntry(f, neighbor_cell));
        }
      }
      ++exp;
    }

    if (_status == PathMatrixCostSearch::SearchStatus::GoalNotFound) return; //vuoldire che abbiamo esplorato tutto il possibile senza aver trovato il goal
    _search_param_changed_flag = false;
  }

}