#pragma once
#include <srrg_data_structures/path_matrix_cost_search.h>

namespace srrg2_navigation_2d {
  using namespace srrg2_core;
  using namespace std;

  class PathMatrixAstarGlobalSearch : public PathMatrixCostSearch {
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW

    void reset();

    bool setStart(const Vector2i& start_);

    int setGoal(const Vector2i& goal_);
  
    void compute();

    PathMatrixCostSearch::SearchStatus getStatus() const {
      return _status;
    }

    const Vector2i& getGoal() const {
      return _goal;
    }

  protected:
    PathSearchQueue _queue;
    PathMatrixCostSearch::SearchStatus _status = PathMatrixCostSearch::SearchStatus::GoalNotFound;
    Vector2i _goal = Vector2i::Zero();

    bool _search_param_changed_flag = true; //questo parametro è ereditato da PathMatrixCostSearch
  };

}