#include "../srrg2_navigation_2d/planner_2d.h"
#include <cmath>
#include <iostream>


namespace srrg2_navigation_2d {
  using namespace srrg2_core;
  using namespace std;

    PathMatrixCostSearch::SearchStatus Planner2D::doAStar(const Vector2i& goal_, const Vector2i& start_, PathMatrix& path_matrix_map_) {

        // dopo aver calcolato Dijkstra su tutte le caselle, assegno come euristica di ogni casella la "distanza" dal goal (cioè il costo calcolato da Dijkstra)
        for (size_t i = 0; i < path_matrix_map_.size(); ++i) {
            PathMatrixCell& cell = path_matrix_map_.at(i);
            cell.heuristic = cell.cost;
        }

        _astar_global_search.setPathMatrix(&path_matrix_map_);
        _astar_global_search.param_cost_polynomial.setValue(_dijkstra_search.param_cost_polynomial.constValue()); //usiamo gli stessi parametri
        _astar_global_search.param_min_distance.setValue(_dijkstra_search.param_min_distance.constValue()); //usiamo gli stessi parametri

        _astar_global_search.reset();

        if (!_astar_global_search.setStart(start_)) {
            cerr << "Not able to set the start position!" << endl;
            return PathMatrixCostSearch::GoalNotFound;
        }

        if (!_astar_global_search.setGoal(goal_)) {
            cerr << "Not able to set the goal position!" << endl;
            return PathMatrixCostSearch::GoalNotFound;
        }
        
        _astar_global_search.compute();

        if (_astar_global_search.getStatus() == PathMatrixCostSearch::SearchStatus::GoalNotFound) _planner_status = GoalUnreachable;
        else _planner_status = Cruising;

        return _astar_global_search.getStatus();
    }

    float Planner2D::computeGlobalAStarPath(StdVectorEigenVector3f& path_, const Vector3f& goal_pose_, PathMatrix& path_matrix_map_, const GridMap2DHeader& map_header_) { // "GridMap2DHeader is just a header and a bunch of properties and functions"
        path_.clear();
        path_.push_back(goal_pose_); // come prima cosa aggiungiamo il goal al path!

        int max_steps = 3000; // se troppo piccolo non riesco a calcolarmi i path più lunghi
        int steps = 0;
        Vector2i current_idx = map_header_.global2indices(goal_pose_.head<2>());
        Vector3f last_pose=goal_pose_; // si parte dal goal
        last_pose.z() = 0; // ci troviamo in 2d!
        float length = 0;
        float delta = 0;

        while (steps < max_steps) {
        
            PathMatrixCell& current = path_matrix_map_.at(current_idx);
            PathMatrixCell* parent = current.parent;
            if (!parent) return 0;
            
            current_idx = path_matrix_map_.pos(parent); // aggiorniamo l'indice corrente per la prossima iterazione
            Vector3f parent_pose = Vector3f::Zero();
            parent_pose.head<2>() = map_header_.indices2global(current_idx);
            parent_pose(2) = 0.f;

            delta=(parent_pose-last_pose).norm();
            length+=delta;
            last_pose=parent_pose;
            path_.push_back(parent_pose); // appendiamo vettori Vector3f, cioè le varie posizioni che incontriamo lungo il path

            if ( &(path_matrix_map_.at(current_idx)) == path_matrix_map_.at(current_idx).parent ) { // se ho appena aggiunto al path la "start position"...
                reverse(path_.begin(), path_.end()); //reverse poichè siamo partiti dal goal
                return length; //siamo arrivati allo start
            }

            ++steps;
        }
        return -1;
    }

}