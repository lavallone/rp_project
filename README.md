# rp_project

## MSc. Artificial Intelligence and Robotics, ay 2021/22
## Robot Programming course's project

Since unfortunately I'm working on ubuntu 20.04 on a virtual machine (and my pc has only two cores), all the operations and computations that I had to do in order to use the navigation stack and ROS took quite a long time.
That's the reason I had to use a shell script ("_launch_navigation.sh_") to run in sequence and at the right time the modules of the navigation stack.

The planner already implemented were the **global** and the **local** one. The global planner is the one that plans a path from a initial position to a goal position without considering any obstacles or the fact that the map or the environment might change. The local planner is instead the one that, starting form the global one and sensing the external environment thanks to laser scans, it's able to modify the original route of the robot and avoid possible obstacles that may appear during the wandering. 

**BASIC IDEA behind this project:**

I wanted to apply the _A* search_ algorithm (studied in the _AI course_) to a robotics application, in this case the motion planning. Since I wanted to see in practice the results of the work using ROS, I exploited the navigation software architecture already provided. The main folders (packages) that I studied and deepened to truly understand their functionalities were _srrg2_core/srrg2_data_structures_ and _srrg2_navigation_2d_. The already implemented global planner uses the Dijkstra algorithm to compute the path. So I decided to implement an other global planner which uses the _A*_ algorithm.

The files where the planner has been implemented are located in the _astar_global_path_ folder of the repo. I also modified, in order to permit the usage of both global planners, the two files _planner_2d.h_ and _planner_2d.cpp_ of the _srrg2_navigation_2d_ ROS package.

**_How to use the code:_**

1. Considering that the _srrg2_workspace_ is configured and built on your laptop, the first thing to do it's to move the _astar_global_path_ folder to the _~/srrg2_workspace/src/srrg2_navigation_2d/srrg2_navigation_2d/src_ folder.
2. Add the line "_astar_global_path_library_" below the line "_srrg2_navigation_2d_library_" inside _catkin_package()_ in the _CMakeLists.txt_ in the folder _~/srrg2_workspace/src/srrg2_navigation_2d/srrg2_navigation_2d_. In the src/ folder of the last mentioned directory there is another _CMakeLists.txt_ file. In this one add the line "_add_subdirectory(astar_global_path)_". 
3. Substitute the files _planner_2d.h_ and _planner_2d.cpp_ of the _~/srrg2_workspace/src/srrg2_navigation_2d/srrg2_navigation_2d/src/srrg2_navigation_2d_ folder with the two files of this repo.
4. Build the workspace with the _catkin build_ command.

> Now when you run the navigation stack there's the possibility of choosing which global planner to use. How can you do it? By simply changing the "_global_planner_type_" parameter described in the _planner_live_ms.conf_ configuration file stored in the _~/srrg2_workspace/src/srrg2_navigation_2d/config_ folder.

