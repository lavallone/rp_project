#!/bin/bash
CONFIG_DIR="home/leo/Desktop/workspaces/RP_ws/srrg2_workspace/src/srrg2_navigation_2d/config"
WSSRRG2="source ~/Desktop/workspaces/RP_ws/srrg2_workspace/devel/setup.bash"
xterm -hold -e "cd $CONFIG_DIR;rosrun stage_ros stageros cappero_laser_odom_diag_2020-05-06-16-26-03.world"&
sleep 5
xterm -hold -e "cd $CONFIG_DIR;$WSSRRG2;rosrun srrg2_map_server map_server cappero_laser_odom_diag_2020-05-06-16-26-03.yaml"&
sleep 20
xterm -hold -e "rviz"&
sleep 85
xterm -hold -e "cd $CONFIG_DIR;$WSSRRG2;rosrun srrg2_executor srrg2_shell run_localizer_live_ms.srrg"&
sleep 120
xterm -hold -e "cd $CONFIG_DIR;$WSSRRG2;rosrun srrg2_executor srrg2_shell run_planner_live_ms_nogui.srrg"&
sleep 150
